use mysql_async::{prelude::*, Pool, Conn};
use serde_json::{json, Value};
use serde::{Deserialize};
use lambda_http::{run, service_fn, Body, Error, Request, Response};
use tracing::{error, info, warn};

#[derive(Deserialize)]
struct RegistrationRequest {
    username: String,
    email: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    info!("Function handler invoked");
    let db_url = "mysql://admin:password@mini5data.cf4o662w2m2p.us-east-1.rds.amazonaws.com:3306/mini5"; 
    let pool = Pool::new(db_url);

    // parse the request body
    let payload: Value = serde_json::from_slice(event.body().as_ref()).unwrap();
    
    let response = process_registration(payload, &pool).await.unwrap();

    Ok(Response::builder()
        .status(200)
        // .header("Content-Type", "application/json")
        .header("Content-Type", "text/html")
        .body(response.to_string().into())
        .expect("failed to render response"))
}

async fn process_registration(event: Value, pool: &Pool) -> Result<Value, Error> {
    // parse the request body
    let request: RegistrationRequest = serde_json::from_value(event)?;
    let username = request.username;
    let email = request.email;

    // get a connection from the pool
    let mut conn = pool.get_conn().await?;

    // register the user
    let result = register_user(&mut conn, &username, &email).await;
    let response_body = match result {
        Ok(message) => json!({ "message": message }),
        Err(e) => json!({ "error": e.to_string() }),
    };

    Ok(response_body)
}

async fn register_user(conn: &mut Conn, username: &str, email: &str) -> Result<String, mysql_async::Error> {
    let params = params! {
        "username" => username,
        "email" => email,
    };

    let user_exists: bool = conn
        .exec_first("SELECT EXISTS(SELECT 1 FROM users WHERE username = :username OR email = :email)", &params)
        .await?
        .unwrap_or(false);

    if user_exists {
        warn!("Duplicated Registration");
        Ok("User already exists".to_string())
    } else {
        conn.exec_drop("INSERT INTO users (username, email) VALUES (:username, :email)", &params).await?;
        info!("Registration successful");
        Ok("User registered successfully".to_string())
    }
}
