# Mini Project6

## Author
Haoyan Li (hl467)

## Introduction

This project contains a Rust-written AWS Lambda function for handling user registrations. The function connects to a MySQL database to verify the uniqueness of usernames and emails and to store user information.
The structure of the project is basically the same with the previous project, but the main difference is that this project is written in Rust. For more detail about the project, please refer to the previous project's github page: https://gitlab.oit.duke.edu/risc_group1/mini5proj 

## Demo Video

For a visual demonstration of how this Rust Lambda function works, please watch our demo video here:
https://youtu.be/n1C6yKvTTLM

## Features

- User registration
- Duplicate checks for usernames and emails
- Logging
- Integration with AWS Lambda and MySQL database

## Tech Stack

- Rust
- AWS Lambda
- MySQL
- `mysql_async` for Rust async database operations
- `tracing` for logging

## Pre-deployment Setup

1. **Install Rust**:
   Ensure Rust and Cargo are installed on your system.

2. **AWS CLI**:
   Install and configure the AWS CLI, making sure you have the permissions to create and deploy Lambda functions.

3. **MySQL Database**:
   Set up your MySQL database and update the database URL in the Lambda function.

## Deployment Guide

1. **Build the Lambda Function**:
   Run the following command in the project root to build the Lambda function:

   ```sh
   cargo build --release --target x86_64-unknown-linux-gnu
   ```

2. **Package the Function**:
   Package the compiled binary and all necessary dependencies into a ZIP file.

3. **Upload to AWS Lambda**:
   Use AWS CLI or the AWS Management Console to upload the ZIP file to Lambda.

4. **Set Environment Variables**:
   Set environment variables like the database connection string in the Lambda configuration.

5. **Set Up IAM Role**:
   Ensure the Lambda function has the necessary permissions to access your MySQL database.

## Usage Instructions

Send a POST request to the Lambda function's URL with a JSON body containing:

 Lambda function's API URL: https://jghjxaxvm0.execute-api.us-east-1.amazonaws.com/default/mini5

```json
{
  "username": "desired_username",
  "email": "email@example.com"
}
```

The function will return the status of the registration or related error messages.
